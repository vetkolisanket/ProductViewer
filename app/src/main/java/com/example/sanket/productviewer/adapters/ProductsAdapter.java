package com.example.sanket.productviewer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.example.sanket.productviewer.R;
import com.example.sanket.productviewer.activities.MainActivity;
import com.example.sanket.productviewer.models.Product;
import com.example.sanket.productviewer.viewholders.ProductHolder;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductHolder> implements Filterable{
    private final WeakReference<Context> mContextWeakReference;
    private List<Product> mProductList;
    private List<Product> mFilteredProducts;

    private ProductFilter mProductFilter;

    public ProductsAdapter(List<Product> items, Context context) {
        this.mProductList = items;
        mFilteredProducts = new ArrayList<>();
        this.mContextWeakReference = new WeakReference<>(context);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent,
                                            int viewType) {
        return new ProductHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_product_item, parent, false), mContextWeakReference);
    }

    @Override
    public void onBindViewHolder(final ProductHolder holder, int position) {
        final Context context = mContextWeakReference.get();
        if (context != null) {
            Product item = mFilteredProducts.get(position);
            holder.tvName.setText(item.name);
            holder.tvPrice.setText(item.price);
            final int resId;
            switch (item.url) {
                case "0":
                    resId = R.drawable.zero;
                    break;
                case "1":
                    resId = R.drawable.one;
                    break;
                case "2":
                    resId = R.drawable.two;
                    break;
                case "3":
                    resId = R.drawable.three;
                    break;
                case "4":
                    resId = R.drawable.four;
                    break;
                default:
                    resId = R.mipmap.ic_launcher;
                    break;
            }

            //new LoadDrawable(holder.ivProduct, context).execute(resId, holder.ivProduct.getWidth(), holder.ivProduct.getHeight());
            Picasso.with(context).load(resId).fit().into(holder.ivProduct);
            /*new Thread(new Runnable() {
                @Override
                public void run() {
                    final Drawable drawable = context.getResources().getDrawable(resId);
                    final ScaleDrawable scaleDrawable = new ScaleDrawable(drawable, Gravity.CENTER, 1, 1);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            holder.ivProduct.setImageDrawable(scaleDrawable);
                        }
                    });
                }
            }).start();*/
        }
    }

    @Override
    public int getItemCount() {
        if (mFilteredProducts == null) {
            return 0;
        }
        return mFilteredProducts.size();
    }

    @Override
    public Filter getFilter() {
        if (mProductFilter == null) {
            mProductFilter = new ProductFilter();
        }
        return mProductFilter;
    }

    private class ProductFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            List<Product> filteredProductList = new ArrayList<>();

            for (int i = 0, size = mProductList.size(); i < size; i++) {
                Product product = mProductList.get(i);
                if(product.name.toLowerCase().contains(constraint.toString().toLowerCase())) {
                    filteredProductList.add(product);
                }
            }
            filterResults.values = filteredProductList;
            filterResults.count = filteredProductList.size();
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilteredProducts = (List<Product>) results.values;
            notifyDataSetChanged();
            Context context = mContextWeakReference.get();
            if (context != null) {
                if (mFilteredProducts.size() == 0) {
                    ((MainActivity) context).onResultsFiltered(true);
                } else {
                    ((MainActivity) context).onResultsFiltered(false);
                }
            }
        }
    }
}
