package com.example.sanket.productviewer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sanket.productviewer.fragments.FeaturedProductFragment;

public class FeaturedProductsAdapter extends FragmentStatePagerAdapter {

    public FeaturedProductsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return FeaturedProductFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 5;
    }
}
