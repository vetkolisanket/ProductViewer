package com.example.sanket.productviewer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.sanket.productviewer.R;
import com.squareup.picasso.Picasso;

public class FeaturedProductFragment extends Fragment {

    public static FeaturedProductFragment newInstance(int pos) {

        Bundle args = new Bundle();
        args.putInt("pos", pos);
        FeaturedProductFragment fragment = new FeaturedProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_featured_product_item, container, false);
        bindView(rootView, getArguments().getInt("pos"));
        return rootView;
    }

    private void bindView(View rootView, int position) {
        ImageView ivProduct = (ImageView) rootView.findViewById(R.id.iv_featured_product);
        int resId;
        switch (position) {
            case 0:
                resId = R.drawable.zero;
                break;
            case 1:
                resId = R.drawable.one;
                break;
            case 2:
                resId = R.drawable.two;
                break;
            case 3:
                resId = R.drawable.three;
                break;
            case 4:
                resId = R.drawable.four;
                break;
            default:
                resId = R.mipmap.ic_launcher;
                break;
        }
        //Drawable drawable = getResources().getDrawable(resId);
        //ivProduct.setImageResource(resId);
        //ivProduct.setImageDrawable(drawable);
        //new LoadDrawable(ivProduct, getContext()).execute(resId, ivProduct.getWidth(), ivProduct.getHeight());
        Picasso.with(getContext()).load(resId).centerCrop().fit().into(ivProduct);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
