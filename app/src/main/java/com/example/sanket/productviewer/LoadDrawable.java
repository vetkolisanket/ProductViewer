package com.example.sanket.productviewer;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.AsyncTask;
import android.util.LruCache;
import android.view.Gravity;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

public class LoadDrawable extends AsyncTask<Integer, Void, Drawable> {

    private WeakReference<ImageView> mIVWeakReference;
    private WeakReference<Context> mContextWeakReference;

    private LruCache<Integer, Drawable> mDrawableLruCache;

    public LoadDrawable(ImageView imageView, Context context) {

        mIVWeakReference = new WeakReference<>(imageView);
        mContextWeakReference = new WeakReference<>(context);
        mDrawableLruCache = new LruCache<>(15);

    }

    @Override
    protected Drawable doInBackground(Integer... params) {
        Context context = mContextWeakReference.get();
        if (context != null && params != null) {
            int resId = params[0];
            int width = params[1];
            int height = params[2];
            Drawable drawable = mDrawableLruCache.get(resId);

            if (drawable == null) {
                drawable = context.getResources().getDrawable(resId);
                ScaleDrawable scaleDrawable = new ScaleDrawable(drawable, Gravity.CENTER, 1, 1);
                //drawable.setBounds(0, 0, width, height);
                mDrawableLruCache.put(resId, scaleDrawable);
            }

            return drawable;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Drawable drawable) {
        super.onPostExecute(drawable);
        Context context = mContextWeakReference.get();
        ImageView imageView = mIVWeakReference.get();
        if (context != null && imageView != null && drawable != null) {
            imageView.setImageDrawable(drawable);
        }
    }
}
