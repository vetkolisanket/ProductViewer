package com.example.sanket.productviewer.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.example.sanket.productviewer.R;
import com.example.sanket.productviewer.adapters.FeaturedProductsAdapter;
import com.example.sanket.productviewer.adapters.ProductsAdapter;
import com.example.sanket.productviewer.models.Product;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private ProductsAdapter mProductsAdapter;
    private FeaturedProductsAdapter mFeaturedProductsAdapter;
    private List<Product> mProductList;
    private Locale mCurrentLocale;

    private ViewPager mVPFeaturedProducts;
    private TextView mTVFeaturedProducts;
    private TextView mTVEmptyView;

    private static final String HINDI_LANGUAGE_CODE = "hi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rvProducts = (RecyclerView) findViewById(R.id.rv_products);
        mVPFeaturedProducts = (ViewPager) findViewById(R.id.vp_featured_products);
        mTVFeaturedProducts = (TextView) findViewById(R.id.tv_featured_products);
        NestedScrollView nsvParent = (NestedScrollView) findViewById(R.id.nsv_parent);
        mTVEmptyView = (TextView) findViewById(R.id.emptyView);

        mFeaturedProductsAdapter = new FeaturedProductsAdapter(getSupportFragmentManager());
        if (mVPFeaturedProducts != null) {
            mVPFeaturedProducts.setAdapter(mFeaturedProductsAdapter);
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.app_name));
        }

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        mProductList = new ArrayList<>();
        List<Product> productList = getProductList();
        mProductsAdapter = new ProductsAdapter(mProductList, this);
        if (productList != null) {
            mProductList.addAll(productList);
            mProductsAdapter.getFilter().filter("");
            if (productList.size() == 0) {
                mTVEmptyView.setVisibility(View.VISIBLE);
            }
        }

        if (rvProducts != null) {
            rvProducts.setNestedScrollingEnabled(false);
            rvProducts.setLayoutManager(gridLayoutManager);
            rvProducts.setAdapter(mProductsAdapter);
        }

        if (nsvParent != null) {
            nsvParent.scrollTo(0, 0);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setIconifiedByDefault(true);
        searchView.setImeOptions(EditorInfo.IME_FLAG_NO_FULLSCREEN);
        searchView.setQueryHint(getString(R.string.search_view_hint));

        final ActionBar actionBar = getSupportActionBar();

        //set listeners
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                /*mProductsAdapter.getFilter().filter(query.trim());
                */
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mProductsAdapter.getFilter().filter(newText.trim());
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actionBar != null) {
                    if (!searchView.isIconified()) {
                        actionBar.setTitle("");
                        mVPFeaturedProducts.setVisibility(View.GONE);
                        mTVFeaturedProducts.setVisibility(View.GONE);
                    } else {
                        actionBar.setTitle(getString(R.string.app_name));
                    }
                }
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if (actionBar != null) {
                    actionBar.setTitle(getString(R.string.app_name));
                    mVPFeaturedProducts.setVisibility(View.VISIBLE);
                    mTVFeaturedProducts.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_english:
                if (mCurrentLocale == null || !mCurrentLocale.getLanguage().equals(Locale.ENGLISH.getLanguage())) {
                    setLocale(Locale.ENGLISH.getLanguage());
                }
                break;
            case R.id.action_hindi:
                if (mCurrentLocale == null || !mCurrentLocale.getLanguage().equals(HINDI_LANGUAGE_CODE)) {
                    setLocale(HINDI_LANGUAGE_CODE);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<Product> getProductList() {
        try {
            InputStream inputStream = getAssets().open("products_info.json");

            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            String productsJsonString = new String(buffer, "UTF-8");

            JSONObject productsJsonObject = new JSONObject(productsJsonString);

            Gson gson = new GsonBuilder().create();
            Type listType = new TypeToken<ArrayList<Product>>() {}.getType();
            return gson.fromJson(productsJsonObject.getJSONArray("products").toString(), listType);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setLocale(String lang) {
        mCurrentLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = mCurrentLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        refresh.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(refresh);
    }

    public void onResultsFiltered(boolean showEmptyView) {
        if (showEmptyView) {
            mTVEmptyView.setVisibility(View.VISIBLE);
        } else {
            mTVEmptyView.setVisibility(View.GONE);
        }
    }
}
