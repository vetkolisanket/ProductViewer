package com.example.sanket.productviewer.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sanket.productviewer.R;

import java.lang.ref.WeakReference;

public class ProductHolder extends RecyclerView.ViewHolder {

    public TextView tvName;
    public TextView tvPrice;
    public ImageView ivProduct;

    public ProductHolder(View itemView, WeakReference<Context> contextWeakReference) {
        super(itemView);



        tvName = (TextView) itemView.findViewById(R.id.tv_name);
        tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
        ivProduct = (ImageView) itemView.findViewById(R.id.iv_product);
    }
}
